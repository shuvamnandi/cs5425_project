import csv
from datetime import datetime, time, timedelta
import logging
import os
import time as tm
import twint
from concurrent.futures import ThreadPoolExecutor


DATA_SET_DIRECTORY = '/Users/shuvamnandi/PycharmProjects/NUS/CS5425_Project/resources/'
MOVIE_TITLES_INPUT_FILE = 'titles_2016.tsv'

TWITTER_OUTPUT_DIRECTORY = '/Users/shuvamnandi/PycharmProjects/NUS/CS5425_Project/resources/output_twitter'
OUTPUT_FILE_PREFIX = 'twitter_tweet_counts_movies_{date_time}'
OUTPUT_FILE_SUFFIX = '.csv'

NUM_THREADS = 5

# Configure Twint
def get_twint_configuration(search_term, search_since, search_until, limit):
    c = twint.Config()
    # c.Username = "realDonaldTrump"
    c.Search = search_term  # "Avengers: Infinity War"
    c.Since = search_since  # "2018-04-16 00:00:00"
    c.Until = search_until  # "2018-04-23 23:59:59"
    c.Store_csv = False
    #c.Output = "tweets_{0}_{1}_{2}_to_{3}.csv".format(datetime.now(), search_term, search_since, search_until)
    c.Store_object = True
    c.Count = True
    # c.Debug = False
    c.Hide_output = True
    c.Limit = limit
    return c


# Run Twitter Search
def run_twitter_search(thread_id, search_term, search_since, search_until, limit=5000):
    # Clean existing output lists on Twint
    twint.output.clean_lists()
    c = get_twint_configuration(search_term, search_since, search_until, limit)
    logging.info('Thread ID: %s, Querying for search term: %s, start date: %s, end date: %s',
                 thread_id, c.Search, c.Since, c.Until)
    start_time = tm.process_time()
    logging.info('Twint Config: %s', c)
    twint.run.Search(c)
    elapsed_time = tm.process_time() - start_time
    logging.info('Time elapsed in query: %s, search term: %s, start date: %s, end date: %s',
                 elapsed_time, c.Search, c.Since, c.Until)
    tweets = twint.output.tweets_list
    logging.info('%s tweets found for query with search term: %s, start date: %s, end date: %s',
                 len(tweets), c.Search, c.Since, c.Until)
    return len(tweets)


def find_twitter_movie_tweet_counts(thread_id, data_set):
    logging.info('**** Thread ID %s started with %s rows in data-set', thread_id, len(data_set))
    output_file_name = OUTPUT_FILE_PREFIX.format(date_time=datetime.now()) + '_' + str(thread_id) + '_' + OUTPUT_FILE_SUFFIX
    output_file_path = os.path.join(TWITTER_OUTPUT_DIRECTORY, output_file_name)
    full_output_file_path = os.path.join(TWITTER_OUTPUT_DIRECTORY, "Full_"+output_file_name)
    all_new_rows = []
    line_count = 0
    for row in data_set:
        imdb_title_id, title_name, release_date_str_orig = row
        logging.info('Line %s, Imdb Title ID: %s, Title Name: %s, Release Date: %s',
                     line_count, imdb_title_id, title_name, release_date_str_orig)
        release_date = datetime.combine(datetime.strptime(release_date_str_orig, '%d %b %Y').date(), time.max)
        days_delta = timedelta(days=14)
        start_date = datetime.combine((release_date-days_delta).date(), time.min)
        start_date_str = start_date.strftime('%Y-%m-%d %H:%M:%S')
        release_date_str = release_date.strftime('%Y-%m-%d %H:%M:%S')
        logging.info('Start Date: %s, End Date: %s', start_date_str, release_date_str)
        #tweet_count = 0
        tweet_count = run_twitter_search(thread_id, title_name,  start_date_str, release_date_str)
        _new_row = (line_count, imdb_title_id, title_name, release_date_str_orig, tweet_count)
        try:
            with open(output_file_path, mode='a') as output_file:
                csv_writer = csv.writer(output_file, delimiter="\t")
                csv_writer.writerow(_new_row)
        except Exception as e:
            logging.info('Exception caught in thread id %s, while writing to %s', thread_id, output_file_path)
            logging.error('Exception message: %s', e.msg)
        all_new_rows.append(_new_row)
        line_count += 1

    try:
        with open(full_output_file_path, mode='w') as full_output_file:
            csv_writer = csv.writer(full_output_file, delimiter="\t")
            csv_writer.writerows(all_new_rows)
    except Exception as e:
        logging.info('Exception caught in thread id %s, while writing to %s', thread_id, full_output_file_path)
        logging.error('Exception message: %s', e.msg)

    logging.info('**** Thread ID %s completed!', thread_id)

def threaded_twitter_extraction():
    input_file_path = os.path.join(DATA_SET_DIRECTORY, MOVIE_TITLES_INPUT_FILE)
    data_set_size = 0
    with open(input_file_path) as input_file:
        csv_reader = csv.reader(input_file, delimiter="\t")
        data_set =[row for row in csv_reader]
        data_set_size = len(data_set)
        logging.info("Input data-set contains: %s", data_set_size)

    if data_set_size > 0:
        data = []
        for _idx in range(NUM_THREADS):
            start_index = int(data_set_size / NUM_THREADS) * _idx
            end_index = int(data_set_size / NUM_THREADS) * (_idx + 1)
            if _idx == NUM_THREADS - 1:
                small_data_set = data_set[start_index:]
            else:
                small_data_set = data_set[start_index:end_index]
            data.append(small_data_set)
            pool = ThreadPoolExecutor(NUM_THREADS)
            #logging.info("Thread id %s is starting with dataset: %s", _idx, small_data_set[0:10])
            #pool.submit(find_twitter_movie_tweet_counts, _idx, small_data_set)
        _idx = 3
        find_twitter_movie_tweet_counts(_idx, data[_idx])
        #pool.shutdown(wait=True)
        logging.info("All done here. Extraction completed!!")
    else:
        logging.info("Data-set empty.. Doing nothing!!! Program terminates here")

def main():
    # filemode='w')

    # Creating an object
    logger = logging.getLogger()

    # Setup Configuration
    logging.basicConfig(  # filename="log_file{0}.log".format(datetime.datetime.now()),
        format='%(asctime)s: \t %(levelname)s \t %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')

    # Setting the threshold of logger to INFO
    logger.setLevel(logging.INFO)
    threaded_twitter_extraction()
    # logging.info('Starting Twitter search....')
    # search_terms = ['Frozen 2', 'Avengers: Infinity War']
    # search_since = ["2019-11-21 23:00:00", "2018-04-23 23:00:00"]
    # search_until = ["2019-11-21 23:59:59", "2018-04-23 23:59:59"]
    # for i in range(len(search_terms)):
    #     logger.debug('Starting Twitter search for: ', search_terms[i], 'search since: ', search_since[i],
    #                  'search until: ', search_until[i])
    #     #run_twitter_search(search_terms[i], search_since[i], search_until[i], limit=1000)


if __name__ == '__main__':
    main()
